package com.testcloud.seleniumcourse.dataprovider.datasets;


import com.testcloud.seleniumcourse.dataprovider.details.Login;
import com.testcloud.seleniumcourse.dataprovider.details.TestHelper;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class LoginDataSet {

    @DataProvider(name = "successfulUserAuthentication", parallel = true)
    public static Object[][] successfulUserAuthentication()
            throws Exception {
        List<TestHelper> testHelperList = new ArrayList<>();
        testHelperList.add(new TestHelper("", new Login("user", "pass")));
        Object[][] objectsList = new Object[testHelperList.size()][];
        int index = 0;
        for (Object flow : testHelperList) {
            objectsList[index] = new Object[]{flow};
            index++;
        }
        return objectsList;
    }

    @DataProvider(name = "unsuccessfulUserAuthentication", parallel = true)
    public static Object[][] unsuccessfulUserAuthentication()
            throws Exception {
        List<TestHelper> testHelperList = new ArrayList<>();
        testHelperList.add(new TestHelper("1", new Login("user", "pass")));

        Object[][] objectsList = new Object[testHelperList.size()][];
        int index = 0;
        for (Object flow : testHelperList) {
            objectsList[index] = new Object[]{flow};
            index++;
        }
        return objectsList;
    }
}
