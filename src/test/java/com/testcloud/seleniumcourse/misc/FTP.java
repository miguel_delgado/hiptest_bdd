package com.testcloud.seleniumcourse.misc;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FTP {

    private static FTPClient ftp = null;

    public FTP(String host, String user, String pwd, String remoteDirectory) throws Exception {
        ftp = new FTPClient();
        int reply;
        ftp.connect(host);
        reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new Exception("Exception in connecting to FTP Server");
        }
        ftp.login(user, pwd);
        ftp.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();
        if (remoteDirectory != null)
            ftp.changeWorkingDirectory(remoteDirectory);

    }

    public FTP(String host, String user, String pwd) throws Exception {
        ftp = new FTPClient();
        int reply;
        ftp.connect(host);
        reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new Exception("Exception in connecting to FTP Server");
        }
        ftp.login(user, pwd);
        ftp.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();

    }


    private static void download(String remoteFilePath, String localFilePath) throws IOException {
        FileOutputStream fos = new FileOutputStream(localFilePath);
        ftp.retrieveFile(remoteFilePath, fos);
    }




    private static void disconnect() {
        if (ftp.isConnected()) {
            try {
                ftp.changeToParentDirectory();
                ftp.logout();
                ftp.disconnect();
            } catch (IOException f) {
                // do nothing as files is already downloaded from FTP server
            }
        }
    }


    public static FTPFile[] listFiles(String remoteDirectory) throws Exception {
        return ftp.listFiles();
    }

    public static void downloadFile(String remoteDirectory, String FileName) {

        try {

            File file = new File("target" + File.separator + "fileDownloaded");
            file.mkdirs();
            if (remoteDirectory != null)
                download(remoteDirectory + FileName,
                        "target" + File.separator + "fileDownloaded" + File.separator + FileName);
            else {
                download(FileName,
                        "target" + File.separator + "fileDownloaded" + File.separator + FileName);
            }
            disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void uploadFile(String localPath, String fileName) {
        try {
            FileInputStream fis = new FileInputStream(localPath);
            ftp.storeFile(fileName, fis);
            disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
