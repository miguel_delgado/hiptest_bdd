package com.testcloud.seleniumcourse.tests.login;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.testcloud.seleniumcourse.config.TestManager;
import com.testcloud.seleniumcourse.dataprovider.datasets.LoginDataSet;
import com.testcloud.seleniumcourse.dataprovider.details.TestHelper;
import org.testng.annotations.Test;

public class UnsuccessfulUserAuthenticationTest extends TestManager{
    


    private static ExtentTest parentTest;

    @Test(testName="Unsuccessful user authentication",groups={"all","login"},
    dataProvider = "unsuccessfulUserAuthentication",dataProviderClass = LoginDataSet.class)
    public void unsuccessfulUserAuthentication(TestHelper testHelper)  throws Exception {

        parentTest = setParentTest(parentTest,"Login");
        ExtentTest child = parentTest.createNode(Scenario.class, "Unsuccessful user authentication" + " " + testHelper.getDataSetNumber());
        test.set(child);
        test.set(child);

        // Given the user is on the login page "driver.get()" "test.get()"
        actionWords.get().theUserIsOnTheLoginPage(driver.get(), test.get());
        // When the user inputs the username "testHelper"
        actionWords.get().theUserInputsTheUsername(testHelper);
        // And the user inputs the password "testHelper"
        actionWords.get().theUserInputsThePassword(testHelper);
        // And the user clicks the Login button
        actionWords.get().theUserClicksTheLoginButton();
        // Then the user should be view a unSuccess message
        actionWords.get().theUserShouldBeViewAUnSuccessMessage();
    }

}