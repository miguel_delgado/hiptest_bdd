package com.testcloud.seleniumcourse.tests.manage;

import com.aventstack.extentreports.ExtentTest;
import com.testcloud.seleniumcourse.dataprovider.details.TestHelper;
import com.testcloud.seleniumcourse.pages.login.LoginPage;
import org.openqa.selenium.WebDriver;

public class ActionWords {


    private WebDriver driver;
    protected InheritableThreadLocal<LoginPage> loginPage = new InheritableThreadLocal<>();
    private ExtentTest test;


    public ActionWords(WebDriver driver) {
        this.driver = driver;
    }

    public void theUserIsOnTheLoginPage(WebDriver webdriver, ExtentTest extentTest) {
        this.driver = webdriver;
        this.test = extentTest;
        loginPage.set(new LoginPage(driver, test));
        loginPage.get().checkTheUserIsOnTheLoginPage();
    }

    public void theUserInputsTheUsername(TestHelper testHelper) {
        loginPage.get().typeInUsernameField(testHelper.getLogin().getUsername());
    }

    public void theUserInputsThePassword(TestHelper testHelper) {
        loginPage.get().typeInPassWordField(testHelper.getLogin().getPassword());
    }

    public void theUserClicksTheLoginButton() {
        loginPage.get().clickOnSubmitButton();
    }

    public void theUserShouldBeAuthenticated() {

    }

    public void theUserShouldBeRedirectedToHomePage() {

    }

    public void theUserShouldBePresentedWithASuccessMessage() {

    }

    public void theUserShouldBeViewAUnSuccessMessage() throws Exception {
        loginPage.get().checkWarnMessage();
    }

}