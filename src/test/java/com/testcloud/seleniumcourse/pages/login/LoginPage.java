package com.testcloud.seleniumcourse.pages.login;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.gherkin.model.And;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.gherkin.model.Then;
import com.aventstack.extentreports.gherkin.model.When;
import com.testcloud.seleniumcourse.config.environment.EvidenceType;
import com.testcloud.seleniumcourse.misc.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;

public class LoginPage {

    private WebDriver driver;
    private WebDriverWait wait;
    private Factory loginElements;
    private ExtentTest reportTest;

    public LoginPage(WebDriver driver, ExtentTest reportTest) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 60);
        this.loginElements = new Factory(driver);
        this.reportTest = reportTest;
    }

    public void typeInUsernameField(String username) {
        try {
            assertThat("Username field is not displayed", loginElements.username.isDisplayed(), equalTo(true));
            loginElements.username.clear();
            loginElements.username.sendKeys(username);
            reportTest.createNode(When.class, "the user inputs the username " + username).pass(Status.PASS.toString());
        } catch (AssertionError | Exception e) {
            reportTest.createNode(When.class, "the user inputs the username " + username).fail(e.getLocalizedMessage());
            throw new RuntimeException(e.getMessage());

        }
    }

    public void typeInPassWordField(String password) {
        try {
            assertThat("Password field is not displayed", loginElements.password.isDisplayed(), equalTo(true));
            loginElements.password.clear();
            loginElements.password.sendKeys(password);
            reportTest.createNode(And.class, "the user inputs the password " + password).pass(Status.PASS.toString());
        } catch (AssertionError | Exception e) {
            reportTest.createNode(And.class, "the user inputs the password " + password).fail(e.getLocalizedMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public void clickOnSubmitButton() {
        try {
            assertThat("Submit button is not displayed", loginElements.submitButton.isDisplayed(), equalTo(true));
            assertThat("Submit button is not enabled", loginElements.submitButton.isEnabled(), equalTo(true));
            loginElements.submitButton.click();
            reportTest.createNode(And.class, "the user clicks the Login button").pass(Status.PASS.toString());
        } catch (AssertionError | Exception e) {
            reportTest.createNode(And.class, "the user clicks the Login button").fail(e.getLocalizedMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public void checkTheUserIsOnTheLoginPage() {
        try {
            assertThat("The user is not in \"Login page\"", driver.getCurrentUrl(), containsString("login"));
            reportTest.createNode(Given.class, "the user is on the login page").pass(Status.PASS.toString());
        } catch (AssertionError | Exception e) {
            reportTest.createNode(Given.class, "the user is on the login page").fail(e.getLocalizedMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public void checkWarnMessage() throws Exception {
        try {
            assertThat("Warn message is not displayed", loginElements.warnMessage.isDisplayed(), equalTo(true));
            assertThat("Warn message is not equal to \"credentials are incorrect :-/\"", loginElements.warnMessage.getText(), equalTo("credentials are incorrect :-/"));
            reportTest.pass("Warn message is equal to \"credentials are incorrect :-/\"");
            reportTest.createNode(Then.class, "the user should be view a unSuccess message").pass(Status.FAIL.toString());
        } catch (AssertionError | Exception e) {
            reportTest.createNode(Then.class, "the user should be view a unSuccess message").fail("Evidence: ", Utils.takeScreenshot(driver, EvidenceType.ERROR));
            throw new RuntimeException(e.getMessage());
        }
    }
}
