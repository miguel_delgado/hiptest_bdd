package com.testcloud.seleniumcourse.pages.login;

public class Constants {

    protected static final String USERNAME_ID = "username";
    protected static final String PASSWORD_ID = "password";
    protected static final String SUBMIT_BUTTON_ID = "login";
    protected static final String WARN_MESSAGE_ID = "warn-message";

}
