package com.testcloud.seleniumcourse.config.environment;


import org.apache.log4j.Logger;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

public class ConfigRunner {

    public static String seleniumGrid;
    public static String browser;
    public static String environment;
    public static String local_success_evidence_path;
    public static String local_error_evidence_path;
    public static String remote_host;
    public static String success_evidence_path;
    public static String remote_success_evidence_path;
    public static String error_evidence_path;
    public static String remote_error_evidence_path;
    public static String ftp_user;
    public static String ftp_password;


    public ConfigRunner() throws Exception {

        Logger logger = Logger.getLogger(ConfigRunner.class.getSimpleName());
        String filename = "config.properties";
        InputStream input = ConfigRunner.class.getClassLoader().getResourceAsStream(filename);
        if (input == null) {
            logger.fatal("Sorry, unable to find " + filename);
            return;
        }
        Properties prop = new Properties();
        prop.load(input);
        setSeleniumGrid(prop, logger);
        setBrowser(prop, logger);
        setEnvironment(prop, logger);
        setEvidences(prop, logger);
        setFtpCredentials(prop, logger);
    }

    private static void setEnvironment(Properties prop, Logger logger) {
        String property = prop.getProperty("environment");
        if (System.getProperty("environment") != null){
            property = System.getProperty("environment");
        }
        switch (property) {
            case "develop":
                environment = prop.getProperty("environment_protocol") + "://" + prop.getProperty("environment_develop");
                break;
            case "test":
                environment = prop.getProperty("environment_protocol") + "://" + prop.getProperty("environment_test");
                break;
            case "production":
                environment = prop.getProperty("environment_protocol") + "://" + prop.getProperty("environment_production");
                break;
            default:
                environment = prop.getProperty("environment_protocol") + "://" + prop.getProperty("environment_test");
                break;
        }
        logger.info("Environment: " + environment);
    }

    private static void setBrowser(Properties prop, Logger logger) {
        browser = prop.getProperty("browser_type");
        logger.info("Browser: " + browser);
    }

    private static void setSeleniumGrid(Properties prop, Logger logger) {
        seleniumGrid = prop.getProperty("grid_protocol") + "://" + prop.getProperty("grid_host")
                + ":" + prop.getProperty("grid_port") + prop.getProperty("grid_path");
        logger.info("Selenium Grid: " + seleniumGrid);
    }

    private static void setEvidences(Properties prop, Logger logger) {
        String protocol = prop.getProperty("protocol_remote_host") + "://";


        local_success_evidence_path = prop.getProperty("local_success_evidence_path");
        new File(local_success_evidence_path).mkdirs();
        logger.info("Local success evidence folder : " + local_success_evidence_path);
        local_error_evidence_path = prop.getProperty("local_error_evidence_path");
        new File(local_error_evidence_path).mkdirs();
        logger.info("Local error evidence folder: " + local_error_evidence_path);
        remote_host = prop.getProperty("remote_host");
        success_evidence_path = prop.getProperty("success_evidence_path");
        error_evidence_path = prop.getProperty("error_evidence_path");

        remote_success_evidence_path = protocol + remote_host + File.separator + success_evidence_path;
        remote_error_evidence_path = protocol + remote_host + File.separator + error_evidence_path;
        logger.info("Remote success evidence folder: " + remote_success_evidence_path);
        logger.info("Remote error evidence folder: " + remote_error_evidence_path);
    }

    private static void setFtpCredentials(Properties prop, Logger logger) {
        ftp_user = prop.getProperty("ftp_user");
        logger.info("FTP user: " + ftp_user);
        ftp_password = prop.getProperty("ftp_password");
        logger.info("FTP password: " + ftp_password);
    }

}

