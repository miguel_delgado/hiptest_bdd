package com.testcloud.seleniumcourse.config;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.testcloud.seleniumcourse.config.environment.ConfigRunner;
import com.testcloud.seleniumcourse.config.report.ExtentManager;
import com.testcloud.seleniumcourse.tests.manage.ActionWords;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MiguelDelgado on 6/2/17.
 */
public class TestManager {

    protected static ExtentReports extent;
    private static List<ExtentTest> parentTestList = new ArrayList<>();
    protected static InheritableThreadLocal<ExtentTest> test = new InheritableThreadLocal<>();
    protected static InheritableThreadLocal<RemoteWebDriver> driver = new InheritableThreadLocal<>();
    protected static InheritableThreadLocal<ActionWords> actionWords = new InheritableThreadLocal<>();

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() throws Exception {
        extent = ExtentManager.createInstance("target/extent.html");
        new ConfigRunner();
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        try {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--kiosk");
            driver.set(new RemoteWebDriver(new URL(ConfigRunner.seleniumGrid), new DesiredCapabilities(ConfigRunner.browser, "", Platform.ANY)));
            driver.get().get(ConfigRunner.environment);
            actionWords.set(new ActionWords(driver.get()));
        } catch (Exception e) {
            throw new RuntimeException("driver not load");
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        if (result.getStatus() == ITestResult.FAILURE) {
            test.get().fail(result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP)
            test.get().skip(result.getThrowable());
        else
            test.get().pass("Test passed");
        extent.flush();
        driver.get().close();

    }


    protected synchronized ExtentTest setParentTest(ExtentTest parentTest, String name) {
        if (parentTestList.isEmpty()) {
            parentTest = extent.createTest(name);
            parentTestList.add(parentTest);
        } else {
            boolean notFound=true;
            for (ExtentTest pTest : parentTestList) {
                parentTest = extent.createTest(name);
                if (pTest.getModel().getName().equals(parentTest.getModel().getName())) {
                    extent.removeTest(parentTest);
                    parentTest = pTest;
                    notFound=false;
                    break;
                }else{
                    extent.removeTest(parentTest);
                }
            }
            if (notFound){
                parentTest = extent.createTest(name);
                parentTestList.add(parentTest);
            }
        }
        return parentTest;
    }


}
