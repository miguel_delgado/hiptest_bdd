hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-diff

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-deleted

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-created

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-renamed

hiptest-publisher --config=src/test/resources/hiptest-publisher.conf --show-actionwords-signature-changed
